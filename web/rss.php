<?php
header('Content-Type: application/rss+xml');
$bdd = new PDO('mysql:host=localhost;dbname=watson;charset=utf8','root','r00t');
// th : PDO('mysql:host=127.0.0.1;dbname=watson;charset=utf8','root','Ironsql14!');
// ol : PDO('mysql:host=192.168.84.67;dbname=watson;charset=utf8','root','r00t');
$articles = $bdd->query('SELECT * FROM tl_liens ORDER BY lien_id DESC LIMIT 0,15');
?>
<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
	<channel>
        <title>Watson</title>
        <description>Actualités</description>
        <link>http://www.watson.local</link>
        <atom:link href="http://www.watson.local/rss.php" rel="self" type="application/rss+xml" />
        <?php while($a = $articles->fetch()) { ?>
        <item>
            <title><?= $a['lien_titre'] ?></title>
            <guid isPermaLink="true">http://www.watson.local/link/<?= $a['lien_id'] ?></guid>            
            <description><?= $a['lien_desc'] ?></description>
        </item>
        <?php } ?>
    </channel>
</rss>